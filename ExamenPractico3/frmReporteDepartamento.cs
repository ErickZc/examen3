﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.Shared;
using ExamenPractico3.Reportes;

namespace ExamenPractico3
{
    public partial class frmReporteDepartamento : Form
    {
        public frmReporteDepartamento()
        {
            InitializeComponent();
        }

        ParameterFields parametros = new ParameterFields();
        ParameterField miParametro = new ParameterField();
        ParameterDiscreteValue valor = new ParameterDiscreteValue();

        private void frmReporteDepartamento_Load(object sender, EventArgs e)
        {
            btnBuscar.BackColor = ColorTranslator.FromHtml("#0275d8");
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            this.miParametro.ParameterValueType = ParameterValueKind.StringParameter;
            this.miParametro.Name = "@cantidadEmpleados";
            this.valor.Value = txtCantidad.Text;
            this.miParametro.CurrentValues.Add(valor);
            this.parametros.Add(miParametro);

            this.crystalReportViewer1.ParameterFieldInfo =  parametros;

            reporteDepartamento rpt = new reporteDepartamento();
            //rpt.SetDatabaseLogon("sa", "1234");
            this.crystalReportViewer1.ReportSource = rpt;

        }
    }
}
