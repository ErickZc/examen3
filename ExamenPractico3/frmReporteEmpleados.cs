﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.Shared;
using ExamenPractico3.Reportes;

namespace ExamenPractico3
{
    public partial class frmReporteEmpleados : Form
    {
        ParameterFields parametros = new ParameterFields();
        ParameterField parametro = new ParameterField();
        ParameterDiscreteValue valor = new ParameterDiscreteValue();
        public frmReporteEmpleados()
        {
            InitializeComponent();
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            this.parametro.ParameterValueType = ParameterValueKind.NumberParameter;
            this.parametro.Name = "@sal";
            this.valor.Value = txtParametro.Text;
            this.parametro.CurrentValues.Add(valor);
            this.parametros.Add(parametro);

            this.crystalReportViewer1.ParameterFieldInfo = parametros;

            rptEmpleados rptEmp = new rptEmpleados();
            this.crystalReportViewer1.ReportSource = rptEmp;

        }

        private void txtParametro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
    }
}
